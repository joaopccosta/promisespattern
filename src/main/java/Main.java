public class Main {

    public static void main(String[] args) {
        Callback printCallback = (string) ->{
            System.out.println(string);
        };

        Callback errorCallback = (randomObject) -> {
            throw new Exception("TEST!");
        };

        Callback catchCallback = (randomObject) -> {
            Exception e = (Exception) randomObject;
            try {
                throw e;
            } catch (Exception e1) {

            }
        };

        var promise = new Promise().accept(errorCallback).reject(printCallback, "There was an error!!!");    }
}
