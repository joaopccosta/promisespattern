interface IPromise{
    IPromise accept(Callback callback);
    IPromise accept(Callback callback, Object parameters);
    IPromise reject(Callback callback);
    IPromise reject(Callback callback, Object parameters);
}
