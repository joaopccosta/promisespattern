public class Promise implements IPromise{
    private Exception storedException;

    public Promise() {
        storedException = null;
    }

    public IPromise accept(Callback callback){
        try {
            callback.execute(null);
        }catch (Exception e){
            this.storedException = e;
        }
        return this;
    }

    public IPromise accept(Callback callback, Object parameters){
        try {
            callback.execute(parameters);
        }catch (Exception e){
            this.storedException = e;
        }
        return this;
    }

    public IPromise reject(Callback callback){
        if(storedException != null){
            storedException = null;
            try {
                callback.execute(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this;
    }

    public IPromise reject(Callback callback, Object parameters){
        if(storedException != null){
            storedException = null;
            try {
                callback.execute(parameters);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this;
    }

}
