import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.concurrent.atomic.AtomicInteger;

public class PromiseTest {


    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void callsAcceptCallback() {
        AtomicInteger invokeCount = new AtomicInteger(0);
        IPromise promise = new Promise();
        Callback callback = (nothing) -> {
            invokeCount.incrementAndGet();
        };

        promise.accept(callback);
        assertThat(invokeCount.get(), is(equalTo(1)));
    }

    @Test
    public void chainedAcceptCallsAreAllowed() {
        AtomicInteger invokeCount = new AtomicInteger(0);
        IPromise promise = new Promise();
        Callback callback = (nothing) -> {
            invokeCount.incrementAndGet();
        };

        promise.accept(callback).accept(callback);
        assertThat(invokeCount.get(), is(equalTo(2)));
    }

    @Test
    public void rejectCallsAreNotInvokedAfterSuccessfulAccept() {

        AtomicInteger invokeCount = new AtomicInteger(0);
        AtomicInteger rejectionCount = new AtomicInteger(0);
        IPromise promise = new Promise();
        Callback callback = (nothing) -> {
            invokeCount.incrementAndGet();
        };
        Callback rejection = (nothing) -> {
            rejectionCount.incrementAndGet();
        };

        promise.accept(callback).reject(rejection);
        assertThat(invokeCount.get(), is(equalTo(1)));
        assertThat(rejectionCount.get(), is(equalTo(0)));
    }

    @Test
    public void rejectCallsAreInvokedAfterFailingAccept() {

        AtomicInteger rejectionCount = new AtomicInteger(0);
        IPromise promise = new Promise();
        Callback callback = (nothing) -> {
            throw new Exception("whoops");
        };
        Callback rejection = (nothing) -> {
            rejectionCount.incrementAndGet();
        };

        promise.accept(callback).reject(rejection);
        assertThat(rejectionCount.get(), is(equalTo(1)));
    }
}
